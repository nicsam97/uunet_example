#include "io/read_multilayer_network.hpp"
#include "community/glouvain2.hpp"
#include "operations/flatten.hpp"
#include <vector>
#include <string>
#include <random>
#include "core/attributes/Attribute.hpp"
#include "networks/WeightedNetwork.hpp"
#include <algorithm>
#include <utility>
#include <lib/somefunc.cpp>
#include <KMeans.cxx>
class tables
{

private:
    std::vector<double> prob_table;
    std::vector<int> alias_table;
    std::vector<const uu::net::Vertex *> nodes; //should be replaced by pointers to nodes inside
public:
    tables(std::vector<double> probabilities, std::vector<const uu::net::Vertex *> _nodes)
    {
        nodes = _nodes;
        int num_of_elements = probabilities.size();
        prob_table = std::vector<double>(num_of_elements);
        alias_table = std::vector<int>(num_of_elements);
        for (int i = 0; i < num_of_elements; ++i)
        {
            prob_table[i] = probabilities[i] * num_of_elements;
        }
        std::vector<int> over_full = std::vector<int>();
        std::vector<int> under_full = std::vector<int>();
        std::vector<int> exactly_full = std::vector<int>();
        int non_exactly_full = 0;
        for (int i = 0; i < num_of_elements; ++i)
        {
            if (prob_table[i] > 1)
            {
                over_full.push_back(i);
                non_exactly_full++;
            }
            else if (prob_table[i] < 1)
            {
                under_full.push_back(i);
                non_exactly_full++;
            }
            else
            {
                exactly_full.push_back(i); //question about if = 1 will really work
            }
        }
        int index_under = 0;
        int index_over = 0;
        while (over_full.size() != 0 && under_full.size() != 0)
        {
            index_under = under_full.back();
            index_over = over_full.back();
            under_full.pop_back();
            over_full.pop_back();
            alias_table[index_under] = index_over;
            prob_table[index_over] += prob_table[index_under] - 1;
            if (prob_table[index_over] > 1)
            {
                over_full.push_back(index_over);
                non_exactly_full++;
            }

            else if (prob_table[index_over] < 1)
            {
                under_full.push_back(index_over);
                non_exactly_full++;
            }
            else
            {
                exactly_full.push_back(index_over);
            }
            //std::cout << "fucking eyh" << std::endl;
        }
        if (over_full.size() != 0)
        {
            for (auto i : over_full)
            {
                prob_table[i] = 1;
            }
        }
        if (under_full.size() != 0)
        {
            for (auto i : under_full)
            {
                prob_table[i] = 1;
            }
        }
        /*
            std::cout << "print prob_table" << std::endl;
            for (auto i : prob_table) {
                std::cout << i << std::endl;
            }
            std::cout << "print alias_table" << std::endl;
            for (auto i : alias_table) {
                std::cout << i << std::endl;
            }
            std::cout << std::endl;
            */
    }
    const uu::net::Vertex *alias_sampling(std::default_random_engine *generator)
    {
        std::uniform_real_distribution<double> distribution(0.0, 1.0);
        double x = distribution(*generator);
        int i = floor(alias_table.size() * x);
        //std::cout << "i:" << i << std::endl;
        //std::cout << "alias_table.size()*x: " << alias_table.size()*x << std::endl;
        double y = alias_table.size() * x - i;
        if (y < prob_table[i])
        {
            //std::cout << nodes[6] << std::endl;
            return nodes[i];
        }
        else
        {
            return nodes[alias_table[i]];
        }
    }
    static void preset_probs_network(uu::net::WeightedNetwork *net, std::unordered_map<const uu::net::Vertex *, tables> &king_map, double p, double q, uu::net::EdgeMode edge_mode = uu::net::EdgeMode::INOUT)
    {
        for (auto node : *net->vertices())
        {
            std::string map_key_first = node->name;
            const uu::net::Vertex *map_key_first_pointer = node;
            std::vector<double> probabilities;
            std::vector<const uu::net::Vertex *> neighbors;
            double scale = 0.0;
            for (auto first_neigh : *net->edges()->neighbors(node, edge_mode))
            {
                double weight = net->get_weight(net->edges()->get(first_neigh, node)).value;
                scale += weight;
                neighbors.push_back(first_neigh);
                probabilities.push_back(weight);
            } // For all neighbours of a node
            scale = 1 / scale;
            for (auto &prob : probabilities)
            {
                prob = prob * scale;
            }
            tables table = tables(probabilities, neighbors);
            king_map.insert({map_key_first_pointer, table});
        }
    }
    static void preset_probs_network_real(uu::net::WeightedNetwork *net, std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>> &king_map, double p, double q, uu::net::EdgeMode edge_mode = uu::net::EdgeMode::INOUT)
    {
        for (auto node : *net->vertices())
        {
            std::string map_key_first = node->name;
            const uu::net::Vertex *map_key_first_pointer = node;
            std::unordered_map<const uu::net::Vertex *, tables> inner = std::unordered_map<const uu::net::Vertex *, tables>();
            for (auto first_neigh : *net->edges()->neighbors(node, edge_mode))
            {

                std::vector<const uu::net::Vertex *> store_neighbs = std::vector<const uu::net::Vertex *>();
                for (auto neigh : *net->edges()->neighbors(node, edge_mode))
                {
                    store_neighbs.push_back(neigh);
                }
                std::string map_key_first = first_neigh->name;
                const uu::net::Vertex *map_key_second_pointer = first_neigh;
                std::vector<double> probabilities;
                std::vector<const uu::net::Vertex *> neighbors;
                double scale = 0.0;
                for (auto second_neigh : *net->edges()->neighbors(first_neigh, edge_mode))
                {
                    if (second_neigh == node)
                    {
                        double weight = net->get_weight(net->edges()->get(first_neigh, second_neigh)).value / p;
                        scale += weight;
                        neighbors.push_back(second_neigh);
                        probabilities.push_back(weight);
                    }
                    else if (std::find(store_neighbs.begin(), store_neighbs.end(), second_neigh) != store_neighbs.end())
                    {
                        double weight = net->get_weight(net->edges()->get(first_neigh, second_neigh)).value;
                        scale += weight;
                        neighbors.push_back(second_neigh);
                        probabilities.push_back(weight);
                    }
                    else
                    {
                        double weight = net->get_weight(net->edges()->get(first_neigh, second_neigh)).value / q;
                        scale += weight;
                        neighbors.push_back(second_neigh);
                        probabilities.push_back(weight);
                    }
                } // For all neighbours of a node
                scale = 1 / scale;
                for (auto &prob : probabilities)
                {
                    prob = prob * scale;
                }
                tables table = tables(probabilities, neighbors);
                //king_map.insert
                //king_map.insert({map_key_first_pointer,});
                //king_map[map_key_first_pointer][map_key_second_pointer] = table;

                //inner.insert(std::make_pair(map_key_second_pointer, table));
                inner.insert({map_key_second_pointer, table});

                //std::cout << "second key: " <<  map_key_second_pointer->name << std::endl;
            }
            king_map.insert(std::make_pair(map_key_first_pointer, inner));
            //std::cout << "first key: " <<  map_key_first_pointer->name << std::endl;
        }
    }

    static void preset_probs_network_real(uu::net::Network *net, std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>> &king_map, double p, double q, uu::net::EdgeMode edge_mode = uu::net::EdgeMode::INOUT)
    {
        for (auto node : *net->vertices())
        {
            std::string map_key_first = node->name;
            const uu::net::Vertex *map_key_first_pointer = node;
            std::unordered_map<const uu::net::Vertex *, tables> inner = std::unordered_map<const uu::net::Vertex *, tables>();
            for (auto first_neigh : *net->edges()->neighbors(node, edge_mode))
            {

                std::vector<const uu::net::Vertex *> store_neighbs = std::vector<const uu::net::Vertex *>();
                for (auto neigh : *net->edges()->neighbors(node, edge_mode))
                {
                    store_neighbs.push_back(neigh);
                }
                std::string map_key_first = first_neigh->name;
                const uu::net::Vertex *map_key_second_pointer = first_neigh;
                std::vector<double> probabilities;
                std::vector<const uu::net::Vertex *> neighbors;
                double scale = 0.0;
                for (auto second_neigh : *net->edges()->neighbors(first_neigh, edge_mode))
                {
                    if (second_neigh == node)
                    {
                        double weight = 1.0 / p;
                        scale += weight;
                        neighbors.push_back(second_neigh);
                        probabilities.push_back(weight);
                    }
                    else if (std::find(store_neighbs.begin(), store_neighbs.end(), second_neigh) != store_neighbs.end())
                    {
                        double weight = 1.0;
                        scale += weight;
                        neighbors.push_back(second_neigh);
                        probabilities.push_back(weight);
                    }
                    else
                    {
                        double weight = 1.0 / q;
                        scale += weight;
                        neighbors.push_back(second_neigh);
                        probabilities.push_back(weight);
                    }
                } // For all neighbours of a node
                scale = 1 / scale;
                for (auto &prob : probabilities)
                {
                    prob = prob * scale;
                }
                tables table = tables(probabilities, neighbors);
                //king_map.insert
                //king_map.insert({map_key_first_pointer,});
                //king_map[map_key_first_pointer][map_key_second_pointer] = table;

                //inner.insert(std::make_pair(map_key_second_pointer, table));
                inner.insert({map_key_second_pointer, table});

                //std::cout << "second key: " <<  map_key_second_pointer->name << std::endl;
            }
            king_map.insert(std::make_pair(map_key_first_pointer, inner));
            //std::cout << "first key: " <<  map_key_first_pointer->name << std::endl;
        }
    }

    static std::vector<std::string> some_random_walks(uu::net::Network *flat_net, std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>> &king_map, std::default_random_engine &generator, const uu::net::Vertex *node_second, int len_rand_walk, int numb_rand_walks)
    {
        const uu::net::Vertex *node_start = node_second;
        std::vector<std::string> output = std::vector<std::string>();
        if (len_rand_walk == 0 || numb_rand_walks == 0)
        {
            return output;
        }
        for (int i = 0; i < numb_rand_walks; ++i)
        {
            node_second = node_start;
            auto node_sentence = std::string();
            auto node_first = flat_net->edges()->neighbors(node_second, uu::net::EdgeMode::INOUT)->get_at_random();
            if (len_rand_walk == 1)
            {
                output.push_back(node_second->name + ". ");
            }
            else
            {
                node_sentence.append(node_second->name);
                for (int j = 0; j < (len_rand_walk - 1); ++j)
                {
                    auto node_tmp = king_map.at(node_first).at(node_second).alias_sampling(&generator);
                    node_sentence.append(" " + node_tmp->name);
                    node_first = node_second;
                    node_second = node_tmp;
                }
                node_sentence += ". ";
                output.push_back(node_sentence);
            }
        }
        return output;
    }
    
    static std::vector<std::string> some_random_walks_MG(uu::net::MultilayerNetwork *multi_net, std::unordered_map<const uu::net::Network *, std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>>>  &super_king_map, 
    std::default_random_engine &generator,double r, int len_rand_walk, int numb_rand_walks) {
        std::vector<std::string> output = std::vector<std::string>();
        std::uniform_real_distribution<double> distribution(0.0, 1.0);
        double x = 1.0;
        if (len_rand_walk == 0 || numb_rand_walks == 0)
        {
            return output;
        }
        for (int i = 0; i < numb_rand_walks; ++i)
        {
            auto node_second = multi_net->actors()->get_at_random();
            auto layer = multi_net->layers()->get_at_random();
            while (!layer->vertices()->contains(node_second)) {
                layer = multi_net->layers()->get_at_random();
            }
            auto node_first = layer->edges()->neighbors(node_second, uu::net::EdgeMode::INOUT)->get_at_random();

            auto node_sentence = std::string();

            if (len_rand_walk == 1)
            {
                output.push_back(node_first->name + ". ");
            }
            else
            {
                node_sentence.append(node_first->name);
                for (int j = 0; j < (len_rand_walk - 1); ++j)
                {
                    if( distribution(generator)<r ) {
                        layer = multi_net->layers()->get_at_random();
                    }
                    while (!layer->vertices()->contains(node_second)) {
                        layer = multi_net->layers()->get_at_random();
                    }
                    node_first = layer->edges()->neighbors(node_second, uu::net::EdgeMode::INOUT)->get_at_random();

                    auto node_tmp = super_king_map.at(layer).at(node_first).at(node_second).alias_sampling(&generator);
                    node_first = node_second;
                    node_sentence.append(" " + node_first->name);
                    node_second = node_tmp;
                    node_sentence.append(" " + node_first->name);
                }
                node_sentence += ". ";
                output.push_back(node_sentence);
            }
        }
        return output;
    }

    static void algo_2_MG(uu::net::MultilayerNetwork *MLG)
    {
        w2v::trainSettings_t trainSettings;
        trainSettings.size = static_cast<uint16_t>(50);
        trainSettings.window = static_cast<uint8_t>(5);
        trainSettings.sample = std::stof("1-e3f");
        // trainSettings.withHS = true;
        trainSettings.negative = static_cast<uint8_t>(10);
        trainSettings.threads = static_cast<uint8_t>(12);
        trainSettings.iterations = static_cast<uint8_t>(5);
        trainSettings.minWordFreq = static_cast<uint16_t>(5);
        trainSettings.alpha = std::stof("0.05");
        trainSettings.withSG = true;
        std::default_random_engine generator;
        int len_rand_walk = 80;
        int numb_rand_walks = 5;
        std::string trainFile = "word2vecinput.txt";
        std::string modelFile = "trained_nodes.w2v";
        std::ofstream myfile;

        //int ii = 0;
        auto embedding_vectors = std::vector<std::vector<float>>(MLG->actors()->size());
        for (auto layer : *MLG->layers())
        {
            //ii = 0;
            //memory leak below?
            auto king_map = std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>>();
            preset_probs_network_real(layer, king_map, 1, 2);

            myfile.open("word2vecinput.txt");
            std::cout << "segfaults before random walks?" << std::endl;
            for (auto node : *layer->vertices())
            {
                std::vector<std::string> some_random_walk = some_random_walks(layer, king_map, generator, node, len_rand_walk, numb_rand_walks);
                for (auto sentence : some_random_walk)
                {
                    myfile << sentence;
                }
            }
            myfile.close();
            auto w2v_model = w2v::somefunc::somefuncTrain(trainFile, modelFile, trainSettings);
            /*
            for (auto nodes_in_layer : *layer->vertices())
            {
                auto a = *(w2v_model.vector(nodes_in_layer->name));
                (embedding_vectors[ii]).insert(embedding_vectors[ii].end(), a.begin(), a.end());
                ii++;
            } */
            KMeans ktest=KMeans(2,10,2,10,w2v_model.map(),&generator);
            ktest.run();
            ktest.print_clusters();
            ktest.summary();


           

            //word2vec on this file
        }
        // std::ofstream sumfile;
        // sumfile.open("Out_vectors");
        // for (auto temp: embedding_vectors) {
        //     for (auto temp2 : temp) {
        //         sumfile << " " << temp2;

        //     }
        //     sumfile <<"."<< std::endl;
        // }
        // sumfile.close();
    }

    static void algo_1_MG(uu::net::MultilayerNetwork *ml_net)
    {
        std::unique_ptr<uu::net::WeightedNetwork> flat_net = std::make_unique<uu::net::WeightedNetwork>("asd");
        flatten_weighted(ml_net->layers()->begin(), ml_net->layers()->end(), flat_net.get());

        auto king_map = std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>>();
        preset_probs_network_real(flat_net.get(), king_map, 1, 2);
        std::default_random_engine generator;

        int len_rand_walk = 80;
        int numb_rand_walks = 6;
        std::ofstream myfile;
        std::string trainFile = "word2vecinput.txt";
        std::string modelFile = "trained_nodes.w2v";
        myfile.open(trainFile);
        for (auto node : *flat_net.get()->vertices())
        {
            std::vector<std::string> some_random_walks = tables::some_random_walks(flat_net.get(), king_map, generator, node, len_rand_walk, numb_rand_walks);
            for (auto sentence : some_random_walks)
            {
                myfile << sentence;
            }
        }
        myfile.close();
        w2v::trainSettings_t trainSettings;
        int size_of_embedding = 7;
        trainSettings.size = static_cast<uint16_t>(size_of_embedding);
        trainSettings.window = static_cast<uint8_t>(5);
        trainSettings.sample = std::stof("1-e3f");
        // trainSettings.withHS = true;
        trainSettings.negative = static_cast<uint8_t>(10);
        trainSettings.threads = static_cast<uint8_t>(12);
        trainSettings.iterations = static_cast<uint8_t>(5);
        trainSettings.minWordFreq = static_cast<uint16_t>(5);
        trainSettings.alpha = std::stof("0.05");
        trainSettings.withSG = true;

        auto w2v_model = w2v::somefunc::somefuncTrain(trainFile, modelFile, trainSettings);
        /*
        auto embedding_vectors = std::vector<std::vector<float>>();
        for (auto actors_in_graph : *ml_net->actors())
        {
            embedding_vectors.push_back(*(w2v_model.vector(actors_in_graph->name)));
        }
        */
        KMeans ktest=KMeans(2,50,size_of_embedding,ml_net->actors()->size(),w2v_model.map(),&generator);
        ktest.run();
        ktest.print_clusters();
        ktest.summary();

    }


    static void algo_3_MG(uu::net::MultilayerNetwork *MLG) 
    {
        auto super_king_map = std::unordered_map<const uu::net::Network *, std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>>>();
        for (auto layer : *MLG->layers())
        {
            auto king_map = std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>>();
            preset_probs_network_real(layer, king_map, 1, 2);
            super_king_map.insert({layer, king_map});
        }
        std::default_random_engine generator;
        // Take the special multigraph random walk
        auto rands = some_random_walks_MG(MLG,super_king_map,generator,0.5,80,1000);


        int size_of_embedding = 7;
        w2v::trainSettings_t trainSettings;
        trainSettings.size = static_cast<uint16_t>(size_of_embedding);
        trainSettings.window = static_cast<uint8_t>(5);
        trainSettings.sample = std::stof("1-e3f");
        // trainSettings.withHS = true;
        trainSettings.negative = static_cast<uint8_t>(10);
        trainSettings.threads = static_cast<uint8_t>(12);
        trainSettings.iterations = static_cast<uint8_t>(5);
        trainSettings.minWordFreq = static_cast<uint16_t>(5);
        trainSettings.alpha = std::stof("0.05");
        trainSettings.withSG = true;

        
        std::ofstream myfile;
        myfile.open("word2vecinput.txt");
        for (auto sentence : rands)
        {
                myfile << sentence;
        }
        myfile.close();

        std::string trainFile = "word2vecinput.txt";
        std::string modelFile = "trained_nodes.w2v";
        auto w2v_model = w2v::somefunc::somefuncTrain(trainFile, modelFile, trainSettings);

    }

 
};
   

