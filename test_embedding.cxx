#include <memory>
#include "networks/MultilayerNetwork.hpp"
#include "community/CommunityStructure.hpp"
//#include "tables.cxx"


class test_embedding
{
  private:

    std::unique_ptr<uu::net::MultilayerNetwork> net;
    const uu::net::Vertex *v1, *v2, *v3, *v4, *v5, *v6, *v7, *v8;
    const uu::net::Network *l1, *l2, *l3, *l4;
    
    void
    Build_Test_Network()
    {
        
        net = std::make_unique<uu::net::MultilayerNetwork>("net");
         // Adding actors

           auto v1 = net->actors()->add("v1");
           auto v2 = net->actors()->add("v2");
           auto v3 = net->actors()->add("v3");
           auto v4 = net->actors()->add("v4");
           auto v5 = net->actors()->add("v5");
           auto v6 = net->actors()->add("v6");
           auto v7 = net->actors()->add("v7");
           auto v8 = net->actors()->add("v8");

           // Adding layers

           auto l1 = net->layers()->add(std::make_unique<uu::net::Network>("l1"));
           auto l2 = net->layers()->add(std::make_unique<uu::net::Network>("l2"));
           auto l3 = net->layers()->add(std::make_unique<uu::net::Network>("l3"));
           auto l4 = net->layers()->add(std::make_unique<uu::net::Network>("l4"));

           // Adding vertices to layers

            for (auto actor: *net->actors())
            {
                l1->vertices()->add(actor);
                l2->vertices()->add(actor);
                l3->vertices()->add(actor);
                l4->vertices()->add(actor);
                
            }
        
        // Adding edges
        
           l1->edges()->add(v1, v2);
           l1->edges()->add(v1, v3);
           l1->edges()->add(v2, v3);
           l1->edges()->add(v4, v5);
           l1->edges()->add(v4, v6);
           l1->edges()->add(v5, v6);
        

        //l1->edges()->add(v1, v4);
        
           l2->edges()->add(v1, v2);
           l2->edges()->add(v1, v3);
           l2->edges()->add(v2, v3);
           l2->edges()->add(v4, v5);
           l2->edges()->add(v4, v6);
           l2->edges()->add(v5, v6);

           l3->edges()->add(v7, v6);
           l3->edges()->add(v8, v7);
           l3->edges()->add(v8, v5);
           l3->edges()->add(v8, v6);
           //l3->edges()->add(v8, v1);

           //l4->edges()->add(v1, v7);
           //l4->edges()->add(v2, v7);
           //l4->edges()->add(v3, v7);
           //l4->edges()->add(v2, v3);
        
        //l2->edges()->add(v1, v5);
    }
    public:
    void Test_Clustering() {
        Build_Test_Network();
        embedding::algo_1_MG(net.get());
    }

    /*
     void
    TearDown() override
    {
        std::remove(test_file_name.data());
    }
     */

};