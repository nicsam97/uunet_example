#include "io/read_multilayer_network.hpp"
#include "community/glouvain2.hpp"
#include "operations/flatten.hpp"
#include <memory>
class test_alias_sampling
{
public:
    static void test()
    {
        std::cout << "Computer testing alias sampling, wait a second: \n" << std::endl;
        std::vector<double> probs = {0.05,0.5, 0.25, 0.1, 0.1};
        std::vector<int> nodes_proxy = {5, 10, 15, 20, 25};
        tables alias_table = tables(probs, nodes_proxy);
        int sample = 0;
        std::vector<int> keep_track_probs = std::vector<int>(probs.size());
        std::default_random_engine generator;
        for (int i = 0; i < 100000; i++)
        {
            sample = alias_table.alias_sampling(&generator);
            //std::cout << sample << std::endl;
            switch (sample)
            {
            case 5:
                keep_track_probs[0] += 1;
                break;
            case 10:
                keep_track_probs[1] += 1;
                break;
            case 15:
                keep_track_probs[2] += 1;
                break;
            case 20:
                keep_track_probs[3] += 1;
                break;
            case 25:
                keep_track_probs[4] += 1;
                break;

            default:
                break;
            }
        }
        for (auto i : keep_track_probs)
        {
            std::cout << i << std::endl;
        }
    }
};
