#include "io/read_multilayer_network.hpp"

#include "community/glouvain2.hpp"
#include "operations/flatten.hpp"
#include <memory>
#include <unordered_map>
#include <string>
#include <vector>
#include <ostream>
#include "EmbeddingAlgorithms.hpp"
// #include "test_embedding.cxx"
int
main()
{
    
     std::cout << "Reading network aucs.txt..." << std::endl;
            
    auto m = uu::net::read_attributed_homogeneous_multilayer_network("../aucs.txt", "aucs", ',',true);
    //Build test net
    std::cout << "Starts algo 2" << std::endl;
    embedding::algo_2_MG(m.get());
    int i = 1;
    //test_embedding test;
    //test.Test_Clustering();
     //auto attributes = flat_net->edges()->attr()->get_double(flat_net->edges()->at(0),"weight");

    //auto weight_matrix = utils::get_weights(flat_net.get());

    //std::unordered_map<std::string, std::vector<double>> map1 = {{"red", std::vector<double>() }};
    //std::unordered_map<const uu::net::Vertex *, std::unordered_map<uu::net::Vertex *, const std::vector<double>>> map2;
    //std::unique_ptr<uu::net::WeightedNetwork> flat_net = std::make_unique<uu::net::WeightedNetwork>("Niclas Network");
    //uu::net::flatten_weighted(m->layers()->begin(), m->layers()->end(), flat_net.get());
    /*
    std::unordered_map<const uu::net::Vertex *,tables> king_map =  std::unordered_map<const uu::net::Vertex *,tables>();
    tables::preset_probs_network(flat_net.get(), king_map, 1,2);
    */

    //auto king_map = std::unordered_map<const uu::net::Vertex *, std::unordered_map<const uu::net::Vertex *, tables>>();
    //tables::preset_probs_network_real(flat_net.get(), king_map, 1,2);
    //std::default_random_engine generator;
    //auto node_first = flat_net->vertices()->get_at_random();
    //auto node_second = flat_net->edges()->neighbors(node_first, uu::net::EdgeMode::INOUT)->get_at_random();
    //auto node_tmp = node_first;
    /*
    for(auto node: *flat_net->vertices()){
        std::cout << node_first->name << std::endl;
        std::cout << node_second->name << std::endl;

        node_tmp = king_map_real.at(node_first).at(node_second).alias_sampling(&generator);
        std::cout << node_tmp->name << std::endl;
        //auto hello = king_map_real.at(node_first);
        node_first = node_second;
        node_second = node_tmp;
    } */
    /*
    int len_rand_walk = 5;
    int numb_rand_walks = 1000;
    std::ofstream myfile;
    myfile.open ("word2vecinput.txt");
    for(auto node: *flat_net->vertices()){  
        std::vector<std::string> some_random_walks = tables::some_random_walks(flat_net.get(), king_map, generator,node,len_rand_walk,numb_rand_walks);
        for (auto sentence : some_random_walks) {
            myfile << sentence;
        }
        
    }
    myfile.close(); */
    
    //tables::algo_2_MG(m.get());


    /*
    w2v::trainSettings_t trainSettings;

    trainSettings.size = static_cast<uint16_t>(100);
    trainSettings.window = static_cast<uint8_t>(5);
    trainSettings.sample = std::stof("1-e3f");
    // trainSettings.withHS = true;
    trainSettings.negative = static_cast<uint8_t>(10);
    trainSettings.threads = static_cast<uint8_t>(12);
    trainSettings.iterations = static_cast<uint8_t>(5);
    trainSettings.minWordFreq = static_cast<uint16_t>(5);
    trainSettings.alpha = std::stof("0.05");
    trainSettings.withSG = true;
    std::string trainFile = "word2vecinput.txt";
    std::string modelFile = "trained_nodes.w2v";
    */



    // auto model_model = w2v::somefunc::somefuncTrain(trainFile, modelFile, trainSettings);


    // std::ofstream outfile ("Output_vectors_in_order.txt");

    // auto vector_with_vectors = std::vector<std::vector<float>>();
    // for(auto actors_in_graph:*m->actors()) {
    //     vector_with_vectors.push_back(*(model_model.vector(actors_in_graph->name)));

    //     for (auto text : *(model_model.vector(actors_in_graph->name)))  {
    //         outfile << " "<< text;  

    //     }
    //     outfile << "."<< std::endl;

    // }

    // outfile.close();
    //std::cout << "Starts algo 2" << std::endl;
    //tables::algo_2_MG(m.get());

    //std::cout << "Starts algo 3" << std::endl;
    //tables::algo_3_MG(m.get());
    
    /*
    for (auto i : weight_matrix) {
        for (auto j: i) {
            std::cout << j << std::endl;
        }
    }*/
    /*
    std::cout << std::endl;
    std::cout << std::endl;
    int hej111 = 5555;
    auto weight_matrix1 = utils::get_weights(flat_net.get());
    */
    /*
    for (auto i : weight_matrix1) {
        for (auto j: i) {
            std::cout << j << std::endl;
        }
    } */
    //std::cout << attributes << std::endl;
    
    //test_alias_sampling::test();
    
    /**  Alias sampling  
     * Generate uniform random sample = x
     * i = round down (nx) + 1, y = nx + 1 - i
     * y < U_i return i
     * else return K_i
     * 
    **/
    /**
     * Table generation (one )
     * 
    **/
    /*
    std::unique_ptr<uu::net::Network> flat_net = std::make_unique<uu::net::Network>("Niclas network");

    uu::net::flatten_unweighted(m->layers()->begin(), m->layers()->end(), flat_net.get());
    */
    
    /*
    std::cout << "start printing Niclas Network XD \n" << std::endl;
    size_t count = 0;
    //int allt = 35;
    //int b = 35*75;
        for (auto edge: *flat_net->edges())
        {
            std::cout << (*edge) << std::endl;
            
            if (count++ > 0) break;
        }
    std::cout << std::endl;

    std::cout << "Network read: " << m->summary() << std::endl;
    
    std::cout << std::endl;

    std::cout << "hello world it is Sirage!" << std::endl; 
    // Some iteration on the network's element
    int a = 5;
    for (auto layer: *m->layers())
    {
        std::cout << "Some edges on layer " << (layer->name) << std::endl;

        size_t count = 0;
        for (auto edge: *layer->edges())
        {
            std::cout << (*edge) << std::endl;
            
            if (count++ > 5) break;
        }
    }
    
    std::cout << std::endl;
    
    std::cout << "Running a clustering algorithm from the library..." << std::endl;
    
    auto clustering = uu::net::glouvain2(m.get(), 1.0);
    
    size_t c_id = 0;
    for (auto cluster: *clustering)
    {
        std::cout << std::endl << "Cluster " << (c_id++) << std::endl;
        
        for (auto vl: *cluster)
        {
            // Printing the vertex and the layers where this vertex is active in this cluster
            std::cout << (vl.v->name) << "@" << (vl.l->name) << std::endl;
        }
    }
    */
    /*
    std::cout << "Test KMeans" << std::endl;
    std::default_random_engine generator;
    std::default_random_engine generator1 = generator;
    std::default_random_engine generator2 = generator;
    uniform_real_distribution<float> distr(0.0,2.0);
    std::cout << "Generating stuff" << std::endl;
    std::unordered_map<std::string,w2v::vector_t> testmap = std::unordered_map<std::string,w2v::vector_t> ();
    for(int i=1; i<=10; i++){
        std::string temp=std::string("U");
        temp.append(to_string(i));
        w2v::vector_t vec=w2v::vector_t(2);
        vec[0]=distr(generator)-1.0;
        vec[1]=distr(generator)-1.0;
        //std::cout << "testpoint " << temp << ": " << vec[0] << " " << vec[1] << std::endl;
        //std::cout << "Adding to map\n";
        testmap.insert({temp,vec});
        //std::cout << "Added to map\n";
    }
    KMeans ktest=KMeans(2,10,2,10,testmap,&generator1);
    //ktest.print_points();
    //ktest.print_means();
    ktest.run();
    //ktest.print_clusters();
    ktest.summary();
    //std::default_random_engine generator1;
    std::cout << "here is between them" << std::endl;
    KMeans qtest=KMeans(2,10,2,10,testmap,&generator2);
    qtest.run();
    //qtest.print_clusters();
    qtest.summary();
    */
    return 0;
}
