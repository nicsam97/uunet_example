/**
 * @file
 * @brief
 * @author Max Fomichev
 * @date 15.02.2017
 * @copyright Apache License v.2 (http://www.apache.org/licenses/LICENSE-2.0)
*/

// #include <getopt.h>

// #include <iostream>
// #include <iomanip>

// #include "mapper.hpp"
// #include "word2vec.hpp"
// #include "wordReader.hpp"
// #include "vocabulary.hpp"
// #include "huffmanTree.hpp"
// #include "nsDistribution.hpp"
// #include "downSampling.hpp"
// #include "trainThread.hpp"
// #include "trainer.hpp"

#include "somefunc.hpp"

namespace w2v {





// static struct option longopts[] = {
//         {"train-file",      required_argument,  nullptr,   'f' },
//         {"model-file",      required_argument,  nullptr,   'o' },
//         {"stop-words-file", required_argument,  nullptr,   'x' },
//         {"size",            required_argument,  nullptr,   's' },
//         {"window",          required_argument,  nullptr,   'w' },
//         {"sample",          required_argument,  nullptr,   'l' },
//         {"with-hs",         no_argument,        nullptr,   'h' },
//         {"negative",        required_argument,  nullptr,   'n' },
//         {"threads",         required_argument,  nullptr,   't' },
//         {"iter",            required_argument,  nullptr,   'i' },
//         {"min-word-freq",   required_argument,  nullptr,   'm' },
//         {"alpha",           required_argument,  nullptr,   'a' },
//         {"with-skip-gram",  no_argument,        nullptr,   'g' },
//         {"word-delimiters", required_argument,  nullptr,   'd' },
//         {"end-of-sentence", required_argument,  nullptr,   'e' },
//         {"verbose",         no_argument,        nullptr,   'v' },
//         { nullptr, 0, nullptr, 0 }
// };





 w2v::w2vModel_t somefunc::somefuncTrain(std::string trainFile, std::string modelFile, w2v::trainSettings_t trainSettings) {
  
    std::string stopWordsFile ;
    bool verbose = true;



    // w2v::trainSettings_t trainSettings;
    
    // trainSettings.size = static_cast<uint16_t>(100);
    // trainSettings.window = static_cast<uint8_t>(5);
    // trainSettings.sample = std::stof("1-e3f");
    // // trainSettings.withHS = true;
    // trainSettings.negative = static_cast<uint8_t>(10);
    // trainSettings.threads = static_cast<uint8_t>(12);
    // trainSettings.iterations = static_cast<uint8_t>(5);
    // trainSettings.minWordFreq = static_cast<uint16_t>(5);
    // trainSettings.alpha = std::stof("0.05");
    // trainSettings.withSG = true;

    // int ch = 0;
    // while ((ch = getopt_long(argc, argv, "f:o:x:s:w:l:hn:t:i:m:a:gd:e:v?", longopts, nullptr)) != -1) {
        // switch (ch) {
          //  case 'f':
            //    trainFile = optarg;
              //  break;
            // case 'o':
            //     modelFile = optarg;
            //     break;
            // case 'x':
            //     stopWordsFile = optarg;
            //     break;
            // case 's':
            //     trainSettings.size = static_cast<uint16_t>(std::stoi(optarg));
            //     break;
            // case 'w':
            //     trainSettings.window = static_cast<uint8_t>(std::stoi(optarg));
            //     break;
            // case 'l':
            //     trainSettings.sample = std::stof(optarg);
            //     break;
            // case 'h':
            //     trainSettings.withHS = true;
            //     break;
            // case 'n':
            //     trainSettings.negative = static_cast<uint8_t>(std::stoi(optarg));
            //     break;
            // case 't':
            //     trainSettings.threads = static_cast<uint8_t>(std::stoi(optarg));
            //     break;
            // case 'i':
            //     trainSettings.iterations = static_cast<uint8_t>(std::stoi(optarg));
            //     break;
            // case 'm':
            //     trainSettings.minWordFreq = static_cast<uint16_t>(std::stoi(optarg));
            //     break;
            // case 'a':
            //     trainSettings.alpha = std::stof(optarg);
            //     break;
            // case 'g':
            //     trainSettings.withSG = true;
            //     break;
            // case 'd':
            //     trainSettings.wordDelimiterChars = optarg;
            //     break;
            // case 'e':
            //     trainSettings.endOfSentenceChars = optarg;
            //     break;
            // case 'v':
            //     verbose = true;
            //     break;
        //     case ':':
        //     case '?':
        //     default:
        //         usage(argv[0]);
        //         return 1;
        // }
    // }

  




    if (verbose) {
        std::cout << "Train data file: " << trainFile << std::endl;
        std::cout << "Output model file: " << modelFile << std::endl;
        std::cout << "Stop-words file: " << stopWordsFile << std::endl;
        std::cout << "Training model: " << (trainSettings.withSG?"Skip-Gram":"CBOW") << std::endl;
        std::cout << "Sample approximation method: ";
        if (trainSettings.withHS) {
            std::cout << "Hierarchical softmax" << std::endl;
        } else {
            std::cout << "Negative sampling with number of negative examples = "
                      << static_cast<int>(trainSettings.negative) << std::endl;
        }
        std::cout << "Number of training threads: " << static_cast<int>(trainSettings.threads) << std::endl;
        std::cout << "Number of training iterations: " << static_cast<int>(trainSettings.iterations) << std::endl;
        std::cout << "Min word frequency: " << static_cast<int>(trainSettings.minWordFreq) << std::endl;
        std::cout << "Vector size: " << static_cast<int>(trainSettings.size) << std::endl;
        std::cout << "Max skip length: " << static_cast<int>(trainSettings.window) << std::endl;
        std::cout << "Threshold for occurrence of words: " << trainSettings.sample << std::endl;
        std::cout << "Starting learning rate: " << trainSettings.alpha << std::endl;
        std::cout << std::endl << std::flush;
    }

    w2v::w2vModel_t model;
    bool trained;
    if (verbose) {
        trained = model.train(trainSettings, trainFile, stopWordsFile,
                              [] (float _percent) {
                                  std::cout << "\rParsing train data... "
                                            << std::fixed << std::setprecision(2)
                                            << _percent << "%" << std::flush;
                              },
                              [] (std::size_t _vocWords, std::size_t _trainWords, std::size_t _totalWords) {
                                  std::cout << std::endl
                                            << "Vocabulary size: " << _vocWords << std::endl
                                            << "Train words: " << _trainWords << std::endl
                                            << "Total words: " << _totalWords << std::endl
                                            << std::endl;
                              },
                              [] (float _alpha, float _percent) {
                                  std::cout << '\r'
                                            << "alpha: "
                                            << std::fixed << std::setprecision(6)
                                            << _alpha
                                            << ", progress: "
                                            << std::fixed << std::setprecision(2)
                                            << _percent << "%"
                                            << std::flush;
                              }
        );
        std::cout << std::endl;
    } else {
        trained = model.train(trainSettings, trainFile, stopWordsFile, nullptr, nullptr, nullptr);
    }
    if (!trained) {
        std::cerr << "Training failed: " << model.errMsg() << std::endl;
        return model;
    }



    return model;
}
}