cmake_minimum_required (VERSION 3.0 FATAL_ERROR)
project (example LANGUAGES CXX)

# Choosing Release as default building type.
if (NOT CMAKE_BUILD_TYPE)
  set (CMAKE_BUILD_TYPE Debug ... FORCE)
endif()

# Make sure you use C++14 at least
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# This generates compile commands for my linter, not needed.
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

# Again, we need some extra things.
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O2 -fno-inline -std=c++14")

# Just in case later we want to have them.
include_directories (include)

# Subtitute the route to uunet in your computer
set(MULTINET_INCLUDES "/usr/local/include/uunet" "/usr/local/include/eigen3" "/usr/local/include/infomap" "/usr/local/include/eclat")

# Include uunet includes.
include_directories (${MULTINET_INCLUDES})

link_directories("/usr/local/lib/")

INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR})


set(PROJECT_ROOT_DIR ${CMAKE_SOURCE_DIR})

#set(CMAKE_BINARY_DIR ${PROJECT_ROOT_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_ROOT_DIR})
 
set(LIBRARY_OUTPUT_PATH ${PROJECT_ROOT_DIR})

set(PROJECT_INCLUDE_DIR ${PROJECT_ROOT_DIR}/include)

if (${CMAKE_SYSTEM_NAME} MATCHES "Linux" OR ${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")
    set(LIBS "-pthread")
endif()

if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s")
endif()


set(PRJ_SRCSs
${PROJECT_SOURCE_DIR}/EmbeddingAlgorithms.hpp
${PROJECT_SOURCE_DIR}/EmbeddingAlgorithms.cpp
${PROJECT_SOURCE_DIR}/PreprocessNetwork.cpp
${PROJECT_SOURCE_DIR}/PreprocessNetwork.hpp
${PROJECT_SOURCE_DIR}/RandomWalks.hpp
${PROJECT_SOURCE_DIR}/RandomWalks.cpp
${PROJECT_SOURCE_DIR}/somefunc.hpp
${PROJECT_SOURCE_DIR}/somefunc.cpp

)

add_library(mylib ${PRJ_SRCSs})


#install(TARGETS ${PROJECT_NAME} DESTINATION mylib)
install(FILES ${PROJECT_SOURCE_DIR}/EmbeddingAlgorithms.hpp DESTINATION mylib)
install(FILES ${PROJECT_SOURCE_DIR}/PreprocessNetwork.hpp DESTINATION mylib)
install(FILES ${PROJECT_SOURCE_DIR}/RandomWalks.hpp DESTINATION mylib)
install(FILES ${PROJECT_SOURCE_DIR}/somefunc.hpp DESTINATION mylib)




set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O2 -fno-inline -std=c++14")
ADD_EXECUTABLE(example main.cxx)
target_link_libraries(example mylib uunet word2vec)
#TARGET_LINK_LIBRARIES(example uunet)
#target_link_libraries(example word2vec )


add_subdirectory(lib)
