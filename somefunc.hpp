#ifndef SOMEFUNC_H
#define SOMEFUNC_H

#include <getopt.h>

#include <iostream>
#include <iomanip>

//#include "mapper.hpp"
#include "word2vec.hpp"
//#include "wordReader.hpp"
//#include "vocabulary.hpp"
//#include "huffmanTree.hpp"
//#include "nsDistribution.hpp"
//#include "downSampling.hpp"
//#include "trainThread.hpp"
//#include "trainer.hpp"


namespace w2v {


class somefunc{
    public:
  
    static w2v::w2vModel_t somefuncTrain(std::string trainFile, std::string modelFile, w2v::trainSettings_t trainSettings);
    };




}

#endif