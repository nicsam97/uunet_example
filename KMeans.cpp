#include <iostream>
#include <vector>
#include <cmath>
#include <random>


using namespace std;

class KMeans{

    public:
    int K, num_of_iterations;
    int dimensions, num_of_points;
    default_random_engine* generator;
    const unordered_map<string,w2v::vector_t>& point_map;  
    
    class Point{        
        public:
        string name;

        Point(){            
            name=string();
        }

        Point(const string & _name){
            name=_name;
        }
    };
    
    class Cluster{
        public:
        int cluster_size;
        int dimensions;
        default_random_engine* generator;
        vector<float> cluster_mean;
        vector<Point*> points_within;
        vector<Point> my_points;

        

        // Basic constructor
        // initializes an empty cluster with random mean
        Cluster(int dims, default_random_engine* gen){
            cluster_size=0;
            dimensions=dims;
            generator=gen;
            cluster_mean=vector<float>(dimensions);
            points_within=vector<Point*>();
            my_points=vector<Point>();
        };

        void add(Point& p){
            points_within.push_back(&p);
            my_points.push_back(p);
            cluster_size+=1;
        }

        void randomize_mean(){
            uniform_real_distribution<float> distribution(0.0,2.0);
            for(auto & value : cluster_mean){
                value=distribution(*generator)-1.0;
            }
        }

        void reset_points(){
            points_within.resize(0);
            my_points.resize(0);
            cluster_size=0;
        }
    };

    vector<std::unique_ptr<Cluster>> K_clusters;
    //vector<Cluster> actual_clusters;
    vector<std::unique_ptr<Point>> all_points;
    //vector<Point> actual_points;
    

    KMeans(int K, int iterations, int vec_dimensions, int num_points, const unordered_map<string,w2v::vector_t>& input_map,
                                                                 default_random_engine* gen) : point_map(input_map){
        this->K=K;
        num_of_iterations=iterations;
        dimensions=vec_dimensions;
        num_of_points=num_points;
        generator=gen;
        for(int i=0; i<K; i++){          
            K_clusters.emplace_back(std::make_unique<Cluster>(dimensions,generator));
        }
        for(auto map_entry : point_map){
            all_points.emplace_back(std::make_unique<Point>(map_entry.first));
        }
    }

    private: float distance(Point& point, Cluster* target){
            float distance=0;
            for(int i=0; i < dimensions; i++){
                float temp = get_position(point)[i] - (target->cluster_mean)[i];
                distance+= (temp*temp);
            }
            return sqrt(distance);    
    }

    private: float distance_cos(Point& point, Cluster* target){
            float length_point=0;
            float length_mean=0;
            for(int i=0; i < dimensions; i++){
                length_point += get_position(point)[i]*get_position(point)[i];
                length_mean += (target->cluster_mean)[i]*(target->cluster_mean)[i];
            }
            length_point=sqrt(length_point);
            length_mean=sqrt(length_mean);

            float distance=0;
            for(int i=0; i < dimensions; i++){
                distance+= (get_position(point)[i]/length_point) * ((target->cluster_mean)[i]/length_mean);
            }
            return sqrt(distance);    
    }

    private: const w2v::vector_t get_position(Point& point){
        return point_map.at(point.name);
    }

    private: void find_join_nearest(Point & point, vector<std::unique_ptr<Cluster>> & clusters){
            Cluster* target = clusters.at(0).get();
            float min_dist = distance(point,target);
            for( auto& cluster : clusters){
                float temp=distance(point, cluster.get());
                if(temp<min_dist){
                    min_dist=temp;
                    target=cluster.get();
                }
            }
            target->add(point);
    }

    private: void find_join_nearest_cos(Point & point, vector<std::unique_ptr<Cluster>> & clusters){
            Cluster* target = clusters.at(0).get();
            float max_dist = distance_cos(point,target);
            for( auto& cluster : clusters){
                float temp=distance(point, cluster.get());
                if(temp>max_dist){
                    max_dist=temp;
                    target=cluster.get();
                }
            }
            target->add(point);
    }

    private: void initialize_mean(Cluster* cluster){
        uniform_real_distribution<float> distribution(0,num_of_points-1);
        int rand_index = distribution(*generator);
        for(int i=0; i<dimensions; i++){
            cluster->cluster_mean[i]=get_position(*all_points[rand_index]).at(i);
        }
    }

    private: void update_mean(Cluster* cluster){            
            if(cluster->cluster_size==0){
            }
            else{
                vector<float> temp (dimensions,0);
                for( auto point : cluster->my_points){
                    for( int i=0; i<dimensions; i++){
                        temp[i]+=(get_position(point))[i];
                    }
                }
                float norm_factor=1.0/cluster->cluster_size;
                for( int i=0; i < dimensions; i++){
                    temp[i]*=norm_factor;
                    (cluster->cluster_mean)[i]=temp[i];
                }
            }
        }; 
    
    public: void run(){
        cout << endl << "Running K-means, K=" << (K) << ","  << (num_of_iterations) << " iterations.\n" << endl;
        //cout << "Initializing clusters\n";
        for(auto& cluster : K_clusters){
            initialize_mean(cluster.get());
        }
        //cout << "Initialized clusters\n";
        for(int i=1; i<=num_of_iterations; i++){
            for( auto & point : all_points){
                find_join_nearest(*point, K_clusters);
            }
            for( auto & cluster : K_clusters){
                update_mean(cluster.get());
                cluster->reset_points();
            } 
        }
        for( auto & point : all_points){
            find_join_nearest(*point, K_clusters);
        }
    };

    public: void run_cos(){
        cout << endl << "Running K-means, K=" << (K) << ","  << (num_of_iterations) << " iterations, cos distance.\n" << endl;
        cout << "Initializing clusters\n";
        for(auto& cluster : K_clusters){
            initialize_mean(cluster.get());
        }
        cout << "Initialized clusters\n";
        for(int i=1; i<=num_of_iterations; i++){
            for( auto & point : all_points){
                find_join_nearest_cos(*point, K_clusters);
            }
            for( auto & cluster : K_clusters){
                update_mean(cluster.get());
                cluster->reset_points();
            }
        }
        for( auto & point : all_points){
            find_join_nearest_cos(*point, K_clusters);
        }
        for( auto & cluster : K_clusters){
            update_mean(cluster.get());
        }
    };

    void print_means(){
        for(int i=1; i<=K; i++){
            cout << "Cluster:" << (i) << ", size:" << (K_clusters[i-1]->cluster_size) << endl;
            cout << "Cluster:" << (i) << ", centroid: [";
            for(auto value : K_clusters[i-1]->cluster_mean){
                cout << (value) << " ";
            }
            cout << "]\n";
        }
    }

    void print_points(){
        cout << "ppoints\n";
        for(auto & point : all_points){
            cout << "Point:" << point->name << " [";
            cout << "]\n";
        }
    }

    void print_cluster(int k){
        cout << "Cluster:" << k << "\n";
        cout << "[ ";
        for(auto point : K_clusters[k]->my_points){
            cout << point.name << " ";
        }
        cout << "]\n";
    }

    void print_clusters(){
        for(int i=0; i<K; i++){
            print_cluster(i);
        }
    }

    public: void summary(){
        cout << endl << "K-means, K=" << (K) << "," << (num_of_iterations) << " iterations.\n" << endl;
        print_means();
    };
};
